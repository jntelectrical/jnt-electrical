JNT Electrical is a 100% Australian family owned and operated business that pride themselves on designing and installing the best PV array solar systems to suit your specific needs.

We understand that solar is a long-term investment and that is why our reputation for high quality products and workmanship is what sets us apart from the rest. All of our installations meet and exceed Australian standards, ensuring you get maximum value, safety and power generation for the lifetime of your system, and this is backed up by our ongoing maintenance and support.

Website: https://jntelectrical.com.au/
